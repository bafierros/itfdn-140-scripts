# !/bin/sh
# Title: recall
# Author: Ben Fierros
# Date: 01/07/2022
# Purpose: recall tips and notes from store database file
# Update: Add usage details, 1/13/2022

# consider adding an if statment to print out usage details if the user passes in a "-h"
# consider adding a log file that includes who ran the script and what arguments that passed in.

# Usage details
if [ "$1" = "-h" ] ; then
    echo "Usage: $0"
    exit 0
fi

# variables
store_data="$HOME/gitlab/itfdn-140-scripts/store.db"
if [ "$#" -eq 0 ] ; then
 more $store_data
else
 # note ${PAGER:-more} allows default pager to be used unless nothing set then more used.
 grep -i "$@" $store_data | ${PAGER:-more}
fi



