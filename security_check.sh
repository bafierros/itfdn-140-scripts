#!/bin/bash
# Title: security_check.sh
# Date: 06/01/2021
# Author: Ben Fierros
# Purpose: Find files with full permissions for "others"
# Update:

# help/usage statement
if [ "$1" = "-h" ]
  then
    echo "This script will search your local system for any directories that grant "Other" full rights."
    echo "usage: $0 [ -h ]" 
    exit 0
fi

## Next define a variable for your log file and then update all the references to point to this variable.

### variables 
sclog=security_check.log

## Define a variable for date, note you must surround the date command in back tics to capture the command output.  
## If you have a quote it then you will only see the word "date" and not the actual date and time.
now=`date`

## Create a header at the top of the security log

echo "*******************************" >> $sclog
echo "**** Start security report ******" >> $sclog

## add hostname to the security log
echo "*** Security report for: $HOSTNAME " >> $sclog

# add date to the security log
echo "*** Report Date: $now " >> $sclog

## Log who was logged into the system when the script was run.
echo "*** Found the following users logged in when this script was run" >> $sclog
who >> $sclog

## Add find command here and redirect the output to $sclog
# Consider that you need find to
# - search for folders and not files
# - search for folder that grant the group "other" read, write and execute "rwx"
# - ignore any content that denies your script access as this meas that "other" has not been granted read, write or execute on the folder folder contents
find / -type d -perm -o+w,o+r,o+x >> $sclog

## add two blank lines
echo >> $sclog
echo >> $sclog
## close log for this run
echo "**** End security report ******" >> $sclog
echo "*******************************" >> $sclog
